-- DO $$BEGIN
--     EXECUTE 'CREATE TABLE public.address_partitioned_' || to_char(NOW() + interval '1 day', 'YYYYMMDD')
--         || ' (CONSTRAINT address_partitioned_check
--             CHECK (created_at >= date '''
--             || (NOW() + interval '1 day')::date || ''' AND created_at < date '''
--             || (NOW() + interval '2 days')::date || ''')) INHERITS (public.address)';
-- END$$;








DO $$BEGIN
    EXECUTE 'CREATE TABLE public.address_partitioned_' || to_char(NOW() + interval '1 day', 'YYYYMMDD')
        || ' (CONSTRAINT address_partitioned_check'
            || ' CHECK (created_at >= date '''
            || (NOW() + interval '1 day')::date || ''' AND created_at < date '''
            || (NOW() + interval '2 days')::date || ''')) INHERITS (public.address)';
END$$;