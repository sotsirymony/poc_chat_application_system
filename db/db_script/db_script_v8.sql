-- Create the new partitioned parent table
CREATE TABLE public.address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp,
    CONSTRAINT address_pkey PRIMARY KEY (id)
);

-- Create the child tables for each partition
CREATE TABLE public.address_january ()
INHERITS (public.address);

CREATE TABLE public.address_february ()
INHERITS (public.address);

-- Create triggers to route data to the appropriate partitions
CREATE OR REPLACE FUNCTION address_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.created_at >= '2023-01-01' AND NEW.created_at < '2023-02-01') THEN
        INSERT INTO public.address_january VALUES (NEW.*);
    ELSIF (NEW.created_at >= '2023-02-01' AND NEW.created_at < '2023-03-01') THEN
        INSERT INTO public.address_february VALUES (NEW.*);
    -- Add more conditions for other months
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Create triggers on the parent table to route inserts
CREATE TRIGGER insert_address_trigger
BEFORE INSERT ON public.address
FOR EACH ROW EXECUTE FUNCTION address_insert_trigger();

-- Insert a value into the partitioned address table
INSERT INTO public.address (street, city, created_at)
VALUES ('Sample Street', 'Sample City', '2023-01-15');
INSERT INTO public.address (street, city, created_at)
VALUES ('Sample Street', 'Sample City', '2023-02-15');
INSERT INTO public.address (street, city, created_at)
VALUES ('Sample Street', 'Sample City', '2023-03-15');