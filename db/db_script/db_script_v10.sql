CREATE TABLE address (    
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
) PARTITION BY RANGE (created_at);

CREATE TABLE address_2023 PARTITION OF address
    FOR VALUES FROM ('2023-01-01') TO ('2024-12-01');
CREATE TABLE address_2024 PARTITION OF address
    FOR VALUES FROM ('2024-12-01') TO ('2025-12-01');