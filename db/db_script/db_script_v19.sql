CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
);

CREATE TABLE address_january () INHERITS (address);
CREATE TABLE address_february () INHERITS (address);
CREATE TABLE address_march () INHERITS (address);
CREATE TABLE address_april () INHERITS (address);
CREATE TABLE address_may () INHERITS (address);
CREATE TABLE address_june () INHERITS (address);
CREATE TABLE address_july () INHERITS (address);
CREATE TABLE address_august () INHERITS (address);
CREATE TABLE address_september () INHERITS (address);
CREATE TABLE address_october () INHERITS (address);
CREATE TABLE address_november () INHERITS (address);
CREATE TABLE address_december () INHERITS (address);


CREATE OR REPLACE FUNCTION insert_into_month_partition()
RETURNS TRIGGER AS $$
BEGIN
    IF EXTRACT(MONTH FROM NEW.created_at) = 1 THEN
        INSERT INTO address_january VALUES (NEW.*);
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 2 THEN
        INSERT INTO address_february VALUES (NEW.*);
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 3 THEN
        INSERT INTO address_march VALUES (NEW.*);
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 4 THEN
        INSERT INTO address_march VALUES (NEW.*);    
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 5 THEN
        INSERT INTO address_march VALUES (NEW.*);
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 6 THEN
        INSERT INTO address_march VALUES (NEW.*);
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 7 THEN
        INSERT INTO address_march VALUES (NEW.*);   
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 8 THEN
        INSERT INTO address_march VALUES (NEW.*);  
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 9 THEN
        INSERT INTO address_march VALUES (NEW.*);      
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 10 THEN
        INSERT INTO address_march VALUES (NEW.*);  
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 11 THEN
        INSERT INTO address_march VALUES (NEW.*);    
    ELSIF EXTRACT(MONTH FROM NEW.created_at) = 12 THEN
        INSERT INTO address_march VALUES (NEW.*);                
    END IF;
    
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER insert_month_partition_trigger
BEFORE INSERT ON address
FOR EACH ROW EXECUTE FUNCTION insert_into_month_partition();

-- comment test
CREATE SEQUENCE address_seq;


-- First, remove the primary key constraint from the existing id column (if any)
ALTER TABLE address DROP CONSTRAINT IF EXISTS address_pkey;

-- Then, alter the column to make it a serial primary key
ALTER TABLE address
    ALTER COLUMN id SET DATA TYPE serial PRIMARY KEY;