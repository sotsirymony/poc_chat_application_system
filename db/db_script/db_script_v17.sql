CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
) PARTITION BY RANGE (EXTRACT(MONTH FROM created_at));

CREATE TABLE address_january PARTITION OF address  FOR VALUES FROM (1) TO (2);
CREATE TABLE address_february PARTITION OF address  FOR VALUES FROM (2) TO (3);
CREATE TABLE address_march PARTITION OF address  FOR VALUES FROM (3) TO (4);
CREATE TABLE address_april PARTITION OF address  FOR VALUES FROM (4) TO (5);
CREATE TABLE address_may PARTITION OF address  FOR VALUES FROM (5) TO (6);
CREATE TABLE address_june PARTITION OF address  FOR VALUES FROM (6) TO (7);
CREATE TABLE address_july PARTITION OF address  FOR VALUES FROM (7) TO (8);
CREATE TABLE address_august PARTITION OF address  FOR VALUES FROM (8) TO (9);
CREATE TABLE address_september PARTITION OF address  FOR VALUES FROM (9) TO (10);
CREATE TABLE address_october PARTITION OF address  FOR VALUES FROM (10) TO (11);
CREATE TABLE address_november PARTITION OF address  FOR VALUES FROM (11) TO (12);
CREATE TABLE address_december PARTITION OF address  FOR VALUES FROM (12) TO (13);


-- v1
-- Function to handle insertion into partitions and deletion from parent
-- CREATE OR REPLACE FUNCTION insert_into_partitions_and_delete_parent()
-- RETURNS TRIGGER AS $$
-- BEGIN
--     IF EXTRACT(MONTH FROM NEW.created_at) = 1 THEN
--         INSERT INTO address_january VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 2 THEN
--         INSERT INTO address_february VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 3 THEN
--         INSERT INTO address_march VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 4 THEN
--         INSERT INTO address_april VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 5 THEN
--         INSERT INTO address_may VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 6 THEN
--         INSERT INTO address_june VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 7 THEN
--         INSERT INTO address_july VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 8 THEN
--         INSERT INTO address_august VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 9 THEN
--         INSERT INTO address_september VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 10 THEN
--         INSERT INTO address_october VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 11 THEN
--         INSERT INTO address_november VALUES (NEW.*);
--     ELSIF EXTRACT(MONTH FROM NEW.created_at) = 12 THEN
--         INSERT INTO address_december VALUES (NEW.*);
--     END IF;
    
--     DELETE FROM address WHERE id = NEW.id;
    
--     RETURN NULL;
-- END;
-- $$ LANGUAGE plpgsql;



-- v2
CREATE OR REPLACE FUNCTION insert_into_partitions_and_delete_parent()
RETURNS TRIGGER AS $$
BEGIN
    CASE EXTRACT(MONTH FROM NEW.created_at)
        WHEN 1 THEN
            INSERT INTO address_january VALUES (NEW.*);
        WHEN 2 THEN
            INSERT INTO address_february VALUES (NEW.*);
        WHEN 3 THEN
            INSERT INTO address_march VALUES (NEW.*);
        WHEN 4 THEN
            INSERT INTO address_april VALUES (NEW.*);
        WHEN 5 THEN
            INSERT INTO address_may VALUES (NEW.*);
        WHEN 6 THEN
            INSERT INTO address_june VALUES (NEW.*);
        WHEN 7 THEN
            INSERT INTO address_july VALUES (NEW.*);
        WHEN 8 THEN
            INSERT INTO address_august VALUES (NEW.*);
        WHEN 9 THEN
            INSERT INTO address_september VALUES (NEW.*);
        WHEN 10 THEN
            INSERT INTO address_october VALUES (NEW.*);
        WHEN 11 THEN
            INSERT INTO address_november VALUES (NEW.*);
        WHEN 12 THEN
            INSERT INTO address_december VALUES (NEW.*);
    END CASE;

    DELETE FROM address WHERE id = NEW.id;
    
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Create a trigger to redirect INSERTs and delete from parent
CREATE TRIGGER redirect_insert_and_delete
BEFORE INSERT ON address
FOR EACH ROW
EXECUTE FUNCTION insert_into_partitions_and_delete_parent();