CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
);

CREATE TABLE address_january () INHERITS (address);
CREATE TABLE address_february () INHERITS (address);
CREATE TABLE address_march () INHERITS (address);
CREATE TABLE address_april () INHERITS (address);
CREATE TABLE address_may () INHERITS (address);
CREATE TABLE address_june () INHERITS (address);
CREATE TABLE address_july () INHERITS (address);
CREATE TABLE address_august () INHERITS (address);
CREATE TABLE address_september () INHERITS (address);
CREATE TABLE address_october () INHERITS (address);
CREATE TABLE address_november () INHERITS (address);
CREATE TABLE address_december () INHERITS (address);



CCREATE OR REPLACE FUNCTION insert_into_partitions_no_delete()
RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        CASE EXTRACT(MONTH FROM NEW.created_at)
            WHEN 1 THEN
                INSERT INTO address_january VALUES (NEW.*);
            WHEN 2 THEN
                INSERT INTO address_february VALUES (NEW.*);
            WHEN 3 THEN
                INSERT INTO address_march VALUES (NEW.*);
            WHEN 4 THEN
                INSERT INTO address_april VALUES (NEW.*);
            WHEN 5 THEN
                INSERT INTO address_may VALUES (NEW.*);
            WHEN 6 THEN
                INSERT INTO address_june VALUES (NEW.*);
            WHEN 7 THEN
                INSERT INTO address_july VALUES (NEW.*);
            WHEN 8 THEN
                INSERT INTO address_august VALUES (NEW.*);
            WHEN 9 THEN
                INSERT INTO address_september VALUES (NEW.*);
            WHEN 10 THEN
                INSERT INTO address_october VALUES (NEW.*);
            WHEN 11 THEN
                INSERT INTO address_november VALUES (NEW.*);
            WHEN 12 THEN
                INSERT INTO address_december VALUES (NEW.*);
        END CASE;

        RETURN NULL; -- Returning NULL prevents the insertion into the parent table
    END IF;
    
    RETURN NULL; -- Returning NULL for non-INSERT operations
END;
$$ LANGUAGE plpgsql;

-- Create the trigger on the parent table
CREATE TRIGGER insert_partition_trigger
BEFORE INSERT ON address
FOR EACH ROW EXECUTE FUNCTION insert_into_partitions_no_delete();

