CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
) PARTITION BY RANGE (EXTRACT(MONTH FROM created_at));

CREATE TABLE address_january PARTITION OF address  FOR VALUES FROM (1) TO (2);
CREATE TABLE address_february PARTITION OF address  FOR VALUES FROM (2) TO (3);
CREATE TABLE address_march PARTITION OF address  FOR VALUES FROM (3) TO (4);
CREATE TABLE address_april PARTITION OF address  FOR VALUES FROM (4) TO (5);
CREATE TABLE address_may PARTITION OF address  FOR VALUES FROM (5) TO (6);
CREATE TABLE address_june PARTITION OF address  FOR VALUES FROM (6) TO (7);
CREATE TABLE address_july PARTITION OF address  FOR VALUES FROM (7) TO (8);
CREATE TABLE address_august PARTITION OF address  FOR VALUES FROM (8) TO (9);
CREATE TABLE address_september PARTITION OF address  FOR VALUES FROM (9) TO (10);
CREATE TABLE address_october PARTITION OF address  FOR VALUES FROM (10) TO (11);
CREATE TABLE address_november PARTITION OF address  FOR VALUES FROM (11) TO (12);
CREATE TABLE address_december PARTITION OF address  FOR VALUES FROM (12) TO (13);