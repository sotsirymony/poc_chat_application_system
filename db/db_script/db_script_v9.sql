-- Create the new partitioned parent table (existing setup)
CREATE TABLE public.address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp,
    CONSTRAINT address_pkey PRIMARY KEY (id)
);

-- Create a function to create a new partitioned table for the given month
CREATE OR REPLACE FUNCTION create_partition_table(table_name text)
RETURNS VOID AS $$
BEGIN
    EXECUTE 'CREATE TABLE IF NOT EXISTS ' || table_name || ' () INHERITS (public.address)';
END;
$$ LANGUAGE plpgsql;

-- Create a function to insert data into the appropriate partition
CREATE OR REPLACE FUNCTION address_insert_trigger()
RETURNS TRIGGER AS $$
DECLARE
    partition_table_name text;
BEGIN
    partition_table_name := 'public.address_' || to_char(NEW.created_at, 'YYYY_MM');
    PERFORM create_partition_table(partition_table_name); -- Ensure partition exists
    
    EXECUTE 'INSERT INTO ' || partition_table_name || ' VALUES (($1).*)'
    USING NEW;
    
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Create a trigger on the parent table to route inserts and create partitions
CREATE TRIGGER insert_address_trigger
BEFORE INSERT ON public.address
FOR EACH ROW EXECUTE FUNCTION address_insert_trigger();

-- Insert values into the partitioned address table
INSERT INTO public.address (street, city, created_at)
VALUES ('Sample Street', 'Sample City', '2023-03-15');

INSERT INTO public.address (street, city, created_at)
VALUES ('Sample Street', 'Sample City', '2023-04-20');