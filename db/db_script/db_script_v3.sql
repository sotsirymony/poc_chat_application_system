-- public.address definition

-- Drop table

-- DROP TABLE public.address;

CREATE TABLE public.address (
	id serial4 NOT NULL,
	street varchar(100) NOT NULL,
	city varchar(45) NOT NULL,
	created_at timestamp,
	CONSTRAINT address_pkey PRIMARY KEY (id)
);

INSERT INTO address (id, street, city) VALUES
(1, 'Happy Street', 'Delhi','2023-08-23 00:00:00'),
(2, 'Down the town Street', 'NY','2023-08-23 00:00:00');

