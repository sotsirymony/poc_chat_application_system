-- Create the new partitioned parent table
CREATE TABLE public.address_partitioned (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp,
    CONSTRAINT address_pkey PRIMARY KEY (id)
);

-- Create the child tables for each partition
CREATE TABLE public.address_january ()
INHERITS (public.address_partitioned);

CREATE TABLE public.address_february ()
INHERITS (public.address_partitioned);

-- Create triggers to route data to the appropriate partitions
CREATE OR REPLACE FUNCTION address_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.created_at >= '2023-01-01' AND NEW.created_at < '2023-02-01') THEN
        INSERT INTO public.address_january VALUES (NEW.*);
    ELSIF (NEW.created_at >= '2023-02-01' AND NEW.created_at < '2023-03-01') THEN
        INSERT INTO public.address_february VALUES (NEW.*);
    -- Add more conditions for other months
    ELSE
        RAISE EXCEPTION 'Date out of range. Ensure appropriate partition exists.';
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Create triggers on the parent table to route inserts
CREATE TRIGGER insert_address_trigger
BEFORE INSERT ON public.address_partitioned
FOR EACH ROW EXECUTE FUNCTION address_insert_trigger();