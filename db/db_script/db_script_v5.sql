-- Step 1: Create the master table without any data.
CREATE TABLE public.address_master (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp,
    CONSTRAINT address_master_pkey PRIMARY KEY (id)
);

-- Step 2: Create partitions for specific date ranges.
CREATE TABLE public.address_2023 PARTITION OF public.address_master FOR VALUES FROM ('2023-01-01') TO ('2024-01-01');
CREATE TABLE public.address_2024 PARTITION OF public.address_master FOR VALUES FROM ('2024-01-01') TO ('2025-01-01');
-- You can create more partitions for different date ranges as needed.

-- Step 3: Create an INSERT trigger to route data into partitions.
CREATE OR REPLACE FUNCTION address_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.created_at >= '2023-01-01' AND NEW.created_at < '2024-01-01') THEN
        INSERT INTO public.address_2023 VALUES (NEW.*);
    ELSIF (NEW.created_at >= '2024-01-01' AND NEW.created_at < '2025-01-01') THEN
        INSERT INTO public.address_2024 VALUES (NEW.*);
    -- Add more conditions for additional partitions.
    ELSE
        RAISE EXCEPTION 'Date out of range. Ensure partitions are set up.';
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Attach the trigger to the main table.
CREATE TRIGGER insert_address_trigger
BEFORE INSERT ON public.address_master
FOR EACH ROW EXECUTE FUNCTION address_insert_trigger();

-- Now, you can insert data into the main table, and it will be routed to the appropriate partitions based on the created_at date.
INSERT INTO public.address (street, city, created_at) VALUES ('123 Main St', 'Cityville', '2023-05-15');