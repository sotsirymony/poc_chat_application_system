CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
) PARTITION BY RANGE (EXTRACT(YEAR FROM created_at), EXTRACT(MONTH FROM created_at));

CREATE TABLE address_january PARTITION OF address  FOR VALUES FROM (NULL, 1) TO (NULL, 2);
CREATE TABLE address_february PARTITION OF address  FOR VALUES FROM (NULL, 2) TO (NULL, 3);
CREATE TABLE address_march PARTITION OF address  FOR VALUES FROM (NULL, 3) TO (NULL, 4);
CREATE TABLE address_april PARTITION OF address  FOR VALUES FROM (NULL, 4) TO (NULL, 5);
CREATE TABLE address_may PARTITION OF address  FOR VALUES FROM (NULL, 5) TO (NULL, 6);
CREATE TABLE address_june PARTITION OF address  FOR VALUES FROM (NULL, 6) TO (NULL, 7);
CREATE TABLE address_july PARTITION OF address  FOR VALUES FROM (NULL, 7) TO (NULL, 8);
CREATE TABLE address_august PARTITION OF address  FOR VALUES FROM (NULL, 8) TO (NULL, 9);
CREATE TABLE address_september PARTITION OF address  FOR VALUES FROM (NULL, 9) TO (NULL, 10);
CREATE TABLE address_october PARTITION OF address  FOR VALUES FROM (NULL, 10) TO (NULL, 11);
CREATE TABLE address_november PARTITION OF address  FOR VALUES FROM (NULL, 11) TO (NULL, 12);
CREATE TABLE address_december PARTITION OF address  FOR VALUES FROM (NULL, 12) TO (NULL, 1);