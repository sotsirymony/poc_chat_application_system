CREATE TABLE address (
    id serial4 NOT NULL,
    street varchar(100) NOT NULL,
    city varchar(45) NOT NULL,
    created_at timestamp not NULL
) PARTITION BY RANGE (created_at);

CREATE TABLE address_january PARTITION OF address
    FOR VALUES FROM ('2023-01-01') TO ('2023-02-01');
CREATE TABLE address_february PARTITION OF address
    FOR VALUES FROM ('2023-02-01') TO ('2023-03-01');
CREATE TABLE address_march PARTITION OF address
    FOR VALUES FROM ('2023-03-01') TO ('2023-04-01');
CREATE TABLE address_april PARTITION OF address
    FOR VALUES FROM ('2023-04-01') TO ('2023-05-01');
CREATE TABLE address_may PARTITION OF address
    FOR VALUES FROM ('2023-05-01') TO ('2023-06-01');
CREATE TABLE address_june PARTITION OF address
    FOR VALUES FROM ('2023-06-01') TO ('2023-07-01');
CREATE TABLE address_july PARTITION OF address
    FOR VALUES FROM ('2023-07-01') TO ('2023-08-01');
CREATE TABLE address_august PARTITION OF address
    FOR VALUES FROM ('2023-08-01') TO ('2023-09-01');
CREATE TABLE address_september PARTITION OF address
    FOR VALUES FROM ('2023-09-01') TO ('2023-10-01');
CREATE TABLE address_october PARTITION OF address
    FOR VALUES FROM ('2023-10-01') TO ('2023-11-01');
CREATE TABLE address_november PARTITION OF address
    FOR VALUES FROM ('2023-11-01') TO ('2023-12-01');
CREATE TABLE address_december PARTITION OF address
    FOR VALUES FROM ('2023-12-01') TO ('2023-12-31'); 
    
-- Change end date to the last day of December
-- Repeat similar adjustments for other partitions