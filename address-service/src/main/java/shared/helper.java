package shared;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

 public interface helper {
        public static Timestamp converter(String arg) {
            String timestampString = arg; // Your timestamp string
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Timestamp returnVar=null;
            try {
                Date parsedDate = inputFormat.parse(timestampString);
                Timestamp timestamp = new Timestamp(parsedDate.getTime());

                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String formattedTimestamp = outputFormat.format(timestamp);
                returnVar= Timestamp.valueOf(formattedTimestamp);

                System.out.println("Formatted Timestamp: " + formattedTimestamp);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return returnVar;
        }
    }

