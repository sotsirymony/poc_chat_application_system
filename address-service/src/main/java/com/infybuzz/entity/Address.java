package com.infybuzz.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id",updatable = false, nullable = false)
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq")
//	@SequenceGenerator(name = "address_seq", sequenceName = "address_seq", allocationSize = 1)
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Column( name = "id", nullable = false  )
@Entity(name= "Address")
@Table(name = "address")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "street")
	private String street;

	@Column(name = "created_at")
	private Timestamp created_at;

	@Column(name = "city")
	private String city;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreated_at(Timestamp created_at){
		this.created_at=created_at;
	}
	public Timestamp getCreated_at(){
		return created_at;
	}

}
