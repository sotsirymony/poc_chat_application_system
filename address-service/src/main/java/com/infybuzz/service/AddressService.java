package com.infybuzz.service;

import com.infybuzz.response.AddressDTO;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infybuzz.entity.Address;
import com.infybuzz.repository.AddressRepository;
import com.infybuzz.request.CreateAddressRequest;
import com.infybuzz.response.AddressResponse;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressService {

	Logger logger = LoggerFactory.getLogger(AddressService.class);
	
	@Autowired
	AddressRepository addressRepository;

	@Transactional
	public AddressResponse createAddress(CreateAddressRequest createAddressRequest) {
		
		Address address = new Address();
		address.setStreet(createAddressRequest.getStreet());
		address.setCity(createAddressRequest.getCity());
		address.setCreated_at(createAddressRequest.getCreated_at());
		
		addressRepository.save(address);
		
		return new AddressResponse(address);
		
	}
	
	public AddressResponse getById (long id) {
		
		logger.info("Inside getById " + id);
		
		Address address = addressRepository.findById(id).get();
		
		return new AddressResponse(address);
	}

	public List<AddressDTO> getAddressesByDateRange(Timestamp startDate, Timestamp endDate) {
		List<Address> addresses = addressRepository.findAddressesByCreatedAt(startDate, endDate);
		return addresses.stream()
				.map(this::convertToDto)
				.collect(Collectors.toList());
	}
	private AddressDTO convertToDto(Address address) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(address, AddressDTO.class);
	}
	
}
