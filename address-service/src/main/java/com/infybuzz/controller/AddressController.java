package com.infybuzz.controller;

import com.infybuzz.request.GetAddressRequest;
import com.infybuzz.response.AddressDTO;
import org.apache.tomcat.jni.Address;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infybuzz.request.CreateAddressRequest;
import com.infybuzz.response.AddressResponse;
import com.infybuzz.service.AddressService;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/address")
public class AddressController {

	@Autowired
	AddressService addressService;

	@PostMapping("/create")
	public AddressResponse createAddress (@RequestBody CreateAddressRequest createAddressRequest) {
		return addressService.createAddress(createAddressRequest);
	}
	
	@GetMapping("/getById/{id}")
	public AddressResponse getById(@PathVariable long id) {
		return addressService.getById(id);
	}

	//
	@GetMapping("/getByDateBetween")
	public List<AddressDTO> getAddressesByDateRange(@RequestBody GetAddressRequest getAddressRequest) {
		Timestamp startDate= getAddressRequest.getStartDate();
		Timestamp endDate= getAddressRequest.getStartDate();
		System.out.print("==========================================================");
		System.out.println(addressService.getAddressesByDateRange(startDate, endDate));
		return addressService.getAddressesByDateRange(startDate, endDate);

	}
	
}
