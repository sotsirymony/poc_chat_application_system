package com.infybuzz.request;
import java.sql.Timestamp;

import static shared.helper.converter;
public class GetAddressRequest {
    public String startDate;

    public GetAddressRequest(String startDate,String endDate) {
        this.startDate = startDate;
        this.endDate=endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return converter(endDate);
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Timestamp getStartDate() {
        return converter(startDate);
    }

    public String endDate;
}
