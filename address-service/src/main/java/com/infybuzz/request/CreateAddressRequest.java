package com.infybuzz.request;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static shared.helper.converter;

public class CreateAddressRequest {

	private String street;

	private String city;

	private String created_at;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreated_at(String created_at) {
		this.created_at=created_at;
	}
	public Timestamp getCreated_at() {
		return converter(created_at);
	}

}
