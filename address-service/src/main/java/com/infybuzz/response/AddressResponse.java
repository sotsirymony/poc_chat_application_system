package com.infybuzz.response;

import com.infybuzz.entity.Address;

import java.text.SimpleDateFormat;


public class AddressResponse {

	private long addressId;

	private String street;

	private String city;
	private String created_at;
	
	public AddressResponse(Address address) {
		this.addressId = address.getId();
		this.street = address.getStreet();
		//convert to fit timestamp to dateString value
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = dateFormat.format(address.getCreated_at());
		//end convert timestamp to string value
		//assign value to created at after convert
		this.created_at=dateString;
		this.city = address.getCity();
	}

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_at() {
		return created_at;
	}

}
