package com.infybuzz.response;

import java.sql.Timestamp;

public class AddressDTO {
    private Long id;
    private String street;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    // Add a public no-argument constructor
    public AddressDTO() {
    }

    public AddressDTO(Long id, String street, String city, Timestamp created_at) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.created_at = created_at;
    }

    private String city;
    private Timestamp created_at;

}
